<?php
class Database{
    private $host = "localhost";
    private $db_name = "cda_projet";
    private $username = "root";
    private $password = "root";
    public $connexion;

    public function getConnection(){

        $this->connexion = null;

        try{
            $this->connexion = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Erreur de connexion : " . $exception->getMessage();
        }

        return $this->connexion;
    }   
}