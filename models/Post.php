<?php

class Post
{
    private $connexion;
    private $table = 'post';

    public $id;
    public $postDate;
    public $content;
    public $user_id;
    public $user_email;
    public $user_password;
    public $user_birthDate;
    public $topic_id;
    public $topic_title;

    public function __construct($db){
        $this->connexion = $db;
    }
    
    public function read(){
        $query = 'SELECT
                p.id,
                p.postDate,
                p.content,
                u.id as user_id,
                u.email as user_email,
                u.password as user_password,
                u.birthDate as user_birthDate,
                t.id as topic_id,
                t.title as topic_title
                FROM ' . $this->table . ' p
                LEFT JOIN user u ON p.user_id = u.id
                LEFT JOIN topic t ON p.topic_id = t.id';
            
        $stmt = $this->connexion->prepare($query);
        $stmt->execute();

        return $stmt;
    }
}