<?php

class User
{
    private $connexion;
    private $table = 'user';

    public $id;
    public $email;
    public $password;
    public $birthDate;

    public function __construct($db){
        $this->connexion = $db;
    }

    public function read(){
        $query = 'SELECT
                u.id,
                u.email,
                u.password,
                u.birthDate
                FROM ' . $this->table . ' u';
            
        $stmt = $this->connexion->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function create(){
        $query = 'INSERT INTO
                ' . $this->table .'
                SET
                email = :email
                password = :password
                birthDate = :birthDate';

        $stmt = $this->connexion->prepare($query);

        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':birthDate', $this->birthDate);

        if ($stmt->execute()){
            return true;
        }

        return false;
    }

    public function update(){
        $query = 'UPDATE
                ' . $this->table .'
                SET
                email = :email
                password = :password
                birthDate = :birthDate
                WHERE
                id = :id';

        $stmt = $this->connexion->prepare($query);

        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':birthDate', $this->birthDate);
        $stmt->bindParam(':id', $this->id);

        if ($stmt->execute()){
            return true;
        }

        return false;
    }

    public function delete(){
        $query = 'DELETE
                FROM ' / $this->table . '
                WHERE id = :id';
            
        $stmt = $this->connexion->prepare($query);

        $stmt->bindParam(':id', $this->id);

        if ($stmt->execute()){
            return true;
        }

        return false;
    }
    
}