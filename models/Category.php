<?php

class Category
{
    private $connexion;
    private $table = 'category';

    public $id;
    public $label;

    public function __construct($db){
        $this->connexion = $db;
    }
   
    public function read(){
        $query = 'SELECT
                c.id,
                c.label,
                FROM ' . $this->table . ' c';
            
        $stmt = $this->connexion->prepare($query);
        $stmt->execute();

        return $stmt;
    }
}
