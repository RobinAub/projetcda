<?php

class Topic
{
    private $connexion;
    private $table = 'topic';

    public $id;
    public $title;
    public $category_id;
    public $category_label;
    public $user_id;
    public $user_email;
    public $user_password;
    public $user_birthDate;

    public function __construct($db){
        $this->connexion = $db;
    }
    
    public function read(){
        $query = 'SELECT
                t.id,
                t.title,
                c.id as category_id,
                c.label as category_label,
                u.id as user_id,
                u.email as user_email,
                u.password as user_password,
                u.birthDate as user_birthDate
                FROM ' . $this->table . ' t
                LEFT JOIN category c ON t.category_id = c.id
                LEFT JOIN user u ON t.user_id = u.id';
            
        $stmt = $this->connexion->prepare($query);
        $stmt->execute();

        return $stmt;
    }
}