# BILAN PROJET CDA - ROBIN AUBERTIN

Voici mon retour sur ce projet fait pour l'intégration de la formation CDA

## Choix & Méthodologie

J'ai choisi d'utiliser le PHP car c'est le langage que je maitrise le mieux, j'ai fait plusieurs projets back en PHP lors de ma formation (Formation Dev Web de Simplon Chambéry). qui était demandé dans le brief, j'ai l'habitude de travailler sur symfony (ou au moins avec Doctrine) et vu qu'on ne pouvait pas utiliser de framework ou ORM je devais utiliser d'autres manières de faire.

Aussi je me suis renseigné sur les API REST, et j'ai décidé d'utiliser Postman pour tester mon code.

Au niveau de la DB, j'ai décidé de la créer en local et j'ai créer des valeurs a la main pour pouvoir tester mon API.

Pour la méthodologie global, j'ai tout d'abord voulu m'insipirer de ce que je connais sur Symfony pour me lancer mais j'ai vite rencontré des problèmes, je ne savais plus comment avancer, et j'ai donc recommencer en apprenant comment créer une API from Scratch. J'ai donc regardé comment m'y prendre sur Internet pour comprendre comment ça marche puis en créer une qui correspond au schéma de donnée.

Au niveau du CRUD j'ai crée toutes les méthodes pour l'entité User mais je n'ai pas eu le temps de le faire pour les autres : je n'ai crée que les méthodes read(). Pour allez au bout, j'aurai juste modifier les méthodes que j'ai crée dans User pour les faire fonctionner sur les autres entités.


## Difficultés Rencontrées

J'ai rencontré mes premières difficultés dès la lecture du brief : je comprenais comment le faire avec un symfony, mais ne l'ayant jamais fait sans, j'etais assez vite perdu. 

Comme je l'ai dit plus haut, dans un premier temps, j'ai éssayé de m'inspirer de ce que j'avais déjà fait avant de me rendre compte que ça ne pouvait pas marcher comme je l'imaginais.

J'ai ensuite eu certaines difficultés a trouver des ressources qui pouvait m'aider a comprendre comment je devais m'y prendre.

## Ressources utilisées et temps passé

Pour les ressources, j'ai regardé beaucoup de live-coding sur youtube de développeur qui crée des API REST afin de comprendre comment ça fonctionne.
J'ai aussi regardé la doc de PHP pour les fonctions que je ne connaissais pas dans les exemples que j'ai trouvé et celle de Postman pour comprendre comment fonctionne le logiciel.

Je n'ai pas pu passé beaucoup de temps sur le projet (d'où certaines méthodes manquantes) : en effet, j'ai pu le commencer en début de semaine et étant en stage actuellement je n'ai pu passer que 4 soirées dessus. J'estime a 12h le temps passé sur ce projet.



Robin AUBERTIN

