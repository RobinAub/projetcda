<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");


if($_SERVER['REQUEST_METHOD'] == 'GET'){
    include_once '../../config/Database.php';
    include_once '../../models/Category.php';

    $database = new Database();
    $db = $database->getConnection();

    $category = new Category($db);
    $stmt = $category->read();

    if($stmt->rowCount() > 0){
        $category_array = array();
        $category_array['data'] = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $category_item = [
                "id" => $id,
                "label" => $label,
            ];

            array_push($category_array['data'], $category_item);
        }

        echo json_encode($category_array);
    }

}else{
    echo json_encode(["message" => "Erreur : mauvaise méthode"]);
}