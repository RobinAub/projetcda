<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");


if($_SERVER['REQUEST_METHOD'] == 'GET'){
    include_once '../../config/Database.php';
    include_once '../../models/Topic.php';

    $database = new Database();
    $db = $database->getConnection();

    $topic = new Topic($db);
    $stmt = $topic->read();

    if($stmt->rowCount() > 0){
        $topic_array = array();
        $topic_array['data'] = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $topic_item = [
                "id" => $id,
                "title" => $title,
                "category_id" => $category_id,
                "category_label" => $category_label,
                "user_id" => $user_id,
                "user_email" => $user_email,
                "user_password" => $user_password,
                "user_birthDate" => $user_birthDate
            ];

            array_push($topic_array['data'], $topic_item);
        }

        echo json_encode($topic_array);
    }

}else{
    echo json_encode(["message" => "Erreur : mauvaise méthode"]);
}