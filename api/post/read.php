<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");


if($_SERVER['REQUEST_METHOD'] == 'GET'){
    include_once '../../config/Database.php';
    include_once '../../models/Post.php';

    $database = new Database();
    $db = $database->getConnection();

    $post = new Post($db);
    $stmt = $post->read();

    if($stmt->rowCount() > 0){
        $post_array = array();
        $post_array['data'] = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $post_item = [
                "id" => $id,
                "postDate" => $postDate,
                "content" => $content,
                "user_id" => $user_id,
                "user_email" => $user_email,
                "user_password" => $user_password,
                "user_birthDate" => $user_birthDate,
                "topic_id" => $topic_id,
                "topic_title" => $topic_title
            ];

            array_push($post_array['data'], $post_item);
        }

        echo json_encode($post_array);
    }

}else{
    echo json_encode(["message" => "Erreur : mauvaise méthode"]);
}