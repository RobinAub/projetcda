<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");


if($_SERVER['REQUEST_METHOD'] == 'PUT'){
    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);

    $data = json_decode(file_get_contents("php://input"));

    $user->id = $data->id;

    $user->email = $data->email;
    $user->password = $data->password;
    $user->birthDate = $data->birthDate;

    if($user->update()){
        echo json_encode(["message" => "User update"]);
    }
    else{
        echo json_encode(["message" => "User n'a pas été update"]);
    }
}

else{
    echo json_encode(["message" => "Erreur : mauvaise méthode"]);
}