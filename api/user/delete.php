<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");


if($_SERVER['REQUEST_METHOD'] == 'DELETE'){
    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);

    $data = json_decode(file_get_contents("php://input"));

    $user->id = $data->id;

    if($user->delete()){
        echo json_encode(["message" => "User delete"]);
    }
    else{
        echo json_encode(["message" => "User n'a pas été delete"]);
    }
}

else{
    echo json_encode(["message" => "Erreur : mauvaise méthode"]);
}