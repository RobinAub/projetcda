<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");


if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);

    $data = json_decode(file_get_contents("php://input"));

    $user->email = $data->email;
    $user->password = $data->password;
    $user->birthDate = $data->birthDate;

    if($user->create()){
        echo json_encode(["message" => "User crée"]);
    }
    else{
        echo json_encode(["message" => "User n'a pas été crée"]);
    }
}

else{
    echo json_encode(["message" => "Erreur : mauvaise méthode"]);
}