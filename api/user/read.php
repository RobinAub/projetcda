<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");


if($_SERVER['REQUEST_METHOD'] == 'GET'){
    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);
    $stmt = $user->read();

    if($stmt->rowCount() > 0){
        $user_array = array();
        $user_array['data'] = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $user_item = [
                "id" => $id,
                "email" => $email,
                "password" => $password,
                "birthDate" => $birthDate
            ];

            array_push($user_array['data'], $user_item);
        }

        echo json_encode($user_array);
    }

}else{
    echo json_encode(["message" => "Erreur : mauvaise méthode"]);
}